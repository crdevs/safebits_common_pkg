<?php

namespace Safebits\Common;

use Illuminate\Support\ServiceProvider;
use Safebits\Common\Commands\CreateEncryptorKey;
use Safebits\Common\Commands\MigrateCommon;

/**
 * Class SafebitsCommonServiceProvider
 * @package Safebits\Common
 */
class SafebitsCommonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Registers provider commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                MigrateCommon::class,
                CreateEncryptorKey::class
            ]);
        }

        // Publishes config file to local configuration path
        $this->publishes([
            __DIR__ . '/Config/safebits_common.php' => config_path('safebits_common.php'),
        ]);
    }
}
