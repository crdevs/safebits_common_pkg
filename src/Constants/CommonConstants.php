<?php

namespace Safebits\Common\Constants;

/**
 * Class CommonConstants
 * @package Safebits\Common\Constants
 */
abstract class CommonConstants
{
    /**
     *
     */
    const X_SAFEBITS_KEY = "X-Safebits-Key";

    /**
     *
     */
    const X_CRYPTO_KEY = "X-Crypto-Key";

    /**
     *
     */
    const X_ANALYTICS_KEY = "X-Analytics-Key";

    /**
     *
     */
    const X_SAFEBITS_NONCE = "X-Safebits-Nonce";

    /**
     *
     */
    const X_CRYPTO_NONCE = "X-Crypto-Nonce";

    /**
     *
     */
    const X_ANALYTICS_NONCE = "X-Analytics-Nonce";

    /**
     *
     */
    const X_SAFEBITS_SIGNATURE = "X-Safebits-Signature";

    /**
     *
     */
    const X_CRYPTO_SIGNATURE = "X-Crypto-Signature";

    /**
     *
     */
    const X_ANALYTICS_SIGNATURE = "X-Analytics-Signature";

    /**
     *
     */
    const X_SAFEBITS_CALLBACK_ID= "X-Safebits-Callback-Id";

    /**
     *
     */
    const X_CRYPTO_CALLBACK_ID = "X-Crypto-Callback-Id";

    /**
     *
     */
    const X_ANALYTICS_CALLBACK_ID = "X-Analytics-Callback-Id";

    /**
     *
     */
    const X_CRYPTO_DIVISION = "X-Crypto-Div";

    /**
     *
     */
    const X_CRYPTO_SYSTEM = "X-Crypto-System";

    /**
     *
     */
    const X_CRYPTO_ACCESS = "X-Crypto-Access";

    /**
     *
     */
    const REQUEST_TYPE_INTERNAL = 1;

    /**
     *
     */
    const REQUEST_TYPE_EXTERNAL = 2;

    /**
     *
     */
    const MISCONFIGURATION_EXCEPTION_CODE = 1;

    /**
     *
     */
    const MISCONFIGURATION_EXCEPTION_MESSAGE = 'Unknown Configuration Error';

    /**
     *
     */
    const INVALID_ACCESS_SIGNATURE_CODE = 2;

    /**
     *
     */
    const INVALID_ACCESS_SIGNATURE_MESSAGE = "Invalid Access Signature";

    /**
     *
     */
    const INVALID_ACCESS_KEY_CODE = 3;

    /**
     *
     */
    const INVALID_ACCESS_KEY_MESSAGE = "Invalid Access Key";

    /**
     *
     */
    const INVALID_REQUEST_ENCODING_CODE = 4;

    /**
     *
     */
    const INVALID_REQUEST_ENCODING_MESSAGE = "Invalid Request Encoding";

    /**
     *
     */
    const INACTIVE_CLIENT_DIVISION_CODE = 5;

    /**
     *
     */
    const INACTIVE_CLIENT_DIVISION_MESSAGE = "Merchant Currently Inactive (MD)";

    /**
     *
     */
    const CREDENTIAL_ENCRYPTER_EXCEPTION_CODE = 6;

    /**
     *
     */
    const CREDENTIAL_ENCRYPTER_EXCEPTION_MESSAGE = "Encryptor was not started (unknown reason)";

    /**
     *
     */
    const UNKNOWN_EXCEPTION_CODE = 500;

    /**
     *
     */
    const UNKNOWN_EXCEPTION_MESSAGE = "Unknown API Error";

    /**
     *
     */
    const HTTP_STATUS_200 = 200;

    /**
     *
     */
    const HTTP_STATUS_400 = 400;
}
