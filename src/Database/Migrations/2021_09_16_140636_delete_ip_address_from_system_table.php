<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class DeleteIpAddressFromSystemTable
 */
class DeleteIpAddressFromSystemTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('md_system', function (Blueprint $table) {
            $table->dropColumn("ipAddress");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('md_system', function (Blueprint $table) {
            $table->string("ipAddress", 250)->after('url')->nullable()->default(null);
        });
    }
}
