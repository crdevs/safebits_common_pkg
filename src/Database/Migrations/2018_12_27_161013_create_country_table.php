<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateCountryTable
 */
class CreateCountryTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_country', function (Blueprint $table) {
            $table->increments('countryId');
            $table->string('name', 30)->nullable(false);
            $table->char('iso2', 2)->nullable(false);
            $table->char('iso3', 3)->nullable(false);
            $table->integer('numCode')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_country');
    }
}
