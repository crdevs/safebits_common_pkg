<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateClientTable
 */
class CreateClientTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_client', function (Blueprint $table) {
            $table->increments('clientId');
            $table->string('name', 100)->nullable(false);
            $table->string('acronym', 5)->nullable(false);
            $table->char('identificationNumber', 2)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_client');
    }
}
