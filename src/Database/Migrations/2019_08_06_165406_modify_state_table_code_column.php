<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;
use Safebits\Common\Database\Seeders\StateTableSeeder;

/**
 * Class ModifyStateTableCodeColumn
 */
class ModifyStateTableCodeColumn extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('md_state', function (Blueprint $table) {
            $table->string('code', 10)->nullable()->default(null)->change();
        });

        // Register values as soon as the migration is executed.
        \Artisan::call('db:seed', array('--class' => StateTableSeeder::class, '--force' => true));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('md_state', function (Blueprint $table) {
            $table->string('code', 10)->change();
        });
    }
}
