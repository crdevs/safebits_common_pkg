<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateClientDivisionConfigurationTable
 */
class CreateClientDivisionConfigurationTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_client_division_configuration', function (Blueprint $table) {
            $table->increments('clientDivisionConfigurationId');
            $table->integer('clientDivisionId')->unsigned();
            $table->string('tag', 100)->nullable(false);
            $table->string('value', 200)->nullable(false);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->foreign('clientDivisionId')->references('clientDivisionId')->on('md_client_division');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_client_division_configuration');
    }
}
