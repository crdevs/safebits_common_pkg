<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class DeleteTimeZoneTable
 */
class DeleteTimeZoneTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->dropIfExists('md_timezone');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection($this->connection)->hasTable('md_timezone')) {
            Schema::connection($this->connection)->create('md_timezone', function (Blueprint $table) {
                $table->increments('timeZoneId');
            });
        }
    }
}
