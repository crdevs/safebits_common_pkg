<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateStateTable
 */
class CreateStateTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_state', function (Blueprint $table) {
            $table->increments('stateId');
            $table->integer('countryId')->unsigned();
            $table->string('name', 100)->nullable(false);
            $table->string('code', 10)->nullable(false);

            $table->foreign('countryId')->references('countryId')->on('md_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_state');
    }
}
