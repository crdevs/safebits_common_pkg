<?php

namespace Safebits\Common\Database\Migrations;

use Illuminate\Database\Migrations\Migration;

/**
 * Class MDMigration
 * @package Safebits\Common\Database\Migrations
 */
class MDMigration extends Migration
{
    /**
     * @var string
     */
    protected $connection;

    /**
     * CreateSystemTable constructor.
     */
    public function __construct()
    {
        $this->connection = config('safebits_common.connection');
    }
}
