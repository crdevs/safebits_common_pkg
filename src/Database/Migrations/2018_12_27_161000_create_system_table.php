<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateSystemTable
 */
class CreateSystemTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_system', function (Blueprint $table) {
            $table->increments('systemId');
            $table->string("name", 100);
            $table->string("url", 250);
            $table->string("path", 100);
            $table->boolean("isActive")->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_system');
    }
}
