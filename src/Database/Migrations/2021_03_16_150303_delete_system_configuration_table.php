<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class DeleteSystemConfigurationTable
 */
class DeleteSystemConfigurationTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->dropIfExists('md_system_configuration');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->create('md_system_configuration', function (Blueprint $table) {
            $table->increments('systemConfigurationId');
            $table->integer('systemId')->unsigned();
            $table->string('tag', 100)->nullable(false);
            $table->binary('value')->nullable(false);
            $table->string('description', 100);

            $table->foreign('systemId')->references('systemId')->on('md_system');
        });
    }
}
