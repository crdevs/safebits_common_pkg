<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class DeleteFeeTypeTable
 */
class DeleteFeeTypeTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->dropIfExists('md_fee_type');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->create('md_fee_type', function (Blueprint $table) {
            $table->increments('feeTypeId');
            $table->string('name', 50)->nullable(false);
        });
    }
}
