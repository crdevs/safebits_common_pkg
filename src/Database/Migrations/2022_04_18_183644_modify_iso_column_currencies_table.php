<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class ModifyIsoColumnCurrenciesTable
 */
class ModifyIsoColumnCurrenciesTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('md_currency', function (Blueprint $table) {
            $table->string('iso', 8)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('md_currency', function (Blueprint $table) {
            // This should never change back to 3 chars, because it will mess up the longer ISOs already in the DB (like USDT, DOGE, etc)
//            $table->string('iso', 3)->change();
        });
    }
}
