<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateFeeConfigTable
 */
class CreateFeeConfigTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_fee_config', function (Blueprint $table) {
            $table->increments('feeConfigId');
            $table->integer('clientDivisionId')->unsigned();
            $table->integer('feeTypeId')->unsigned();
            $table->decimal('feeAmount', 60, 30);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->foreign('clientDivisionId')->references('clientDivisionId')->on('md_client_division');
            $table->foreign('feeTypeId')->references('feeTypeId')->on('md_fee_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_fee_config');
    }
}
