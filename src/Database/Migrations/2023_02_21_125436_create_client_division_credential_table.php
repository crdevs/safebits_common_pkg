<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateClientDivisionTable
 */
class CreateClientDivisionCredentialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_client_division_credential', function (Blueprint $table) {
            $table->increments('clientDivisionCredentialId');
            $table->integer('clientDivisionId')->unsigned();
            $table->string('key')->unique();
            $table->binary('secret')->nullable();
            $table->boolean('isActive')->default(true);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->foreign('clientDivisionId')->references('clientDivisionId')->on('md_client_division');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('md_client_division_credential');
    }
}
