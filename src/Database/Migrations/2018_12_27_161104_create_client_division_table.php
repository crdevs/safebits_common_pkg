<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateClientDivisionTable
 */
class CreateClientDivisionTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_client_division', function (Blueprint $table) {
            $table->increments('clientDivisionId');
            $table->integer('clientId')->unsigned();
            $table->string('name', 50)->nullable(false);
            $table->string('acronym', 10)->nullable(false);
            $table->char('identificationNumber', 2)->nullable(false);

            $table->foreign('clientId')->references('clientId')->on('md_client');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_client_division');
    }
}
