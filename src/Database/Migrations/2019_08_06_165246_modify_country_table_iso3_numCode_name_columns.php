<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Common\Database\Migrations\MDMigration;
use Safebits\Common\Database\Seeders\CountryTableSeeder;

/**
 * Class ModifyCountryTableIso3NumCodeNameColumns
 */
class ModifyCountryTableIso3NumCodeNameColumns extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('md_country', function (Blueprint $table) {
            $table->string('iso3', 3)->nullable()->default(null)->change();
            $table->string('numCode')->nullable()->default(null)->change();
            $table->string('name', 50)->nullable()->default(null)->change();
        });

        // Register values as soon as the migration is executed.
        \Artisan::call('db:seed', array('--class' => CountryTableSeeder::class, '--force' => true));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('md_country', function (Blueprint $table) {
            $table->string('iso3', 3)->change();
            $table->string('numCode')->change();
            $table->string('name', 50)->nullable(false)->change();
        });
    }
}
