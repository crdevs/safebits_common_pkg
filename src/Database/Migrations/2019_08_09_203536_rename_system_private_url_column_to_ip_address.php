<?php

use Illuminate\Database\Schema\Blueprint;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class RenameSystemPrivateUrlColumnToIpAddress
 */
class RenameSystemPrivateUrlColumnToIpAddress extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('md_system', function (Blueprint $table) {
            $table->renameColumn('privateUrl', 'ipAddress');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('md_system', function (Blueprint $table) {
            $table->renameColumn('ipAddress', 'privateUrl');
        });
    }
}
