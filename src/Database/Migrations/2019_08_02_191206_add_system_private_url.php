<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class AddSystemPrivateUrl
 */
class AddSystemPrivateUrl extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('md_system', function (Blueprint $table) {
            $table->string("privateUrl", 250)->after('url')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('md_system', function (Blueprint $table) {
            $table->dropColumn("privateUrl");
        });
    }
}
