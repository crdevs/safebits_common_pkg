<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class AddIsNativeColumnToCurrenciesTable
 */
class AddIsNativeColumnToCurrenciesTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('md_currency', function (Blueprint $table) {
            $table->boolean('isNative')->after('isActive')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('md_currency', function (Blueprint $table) {
            $table->dropColumn('isNative');
        });
    }
}
