<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class AddNetworkColumnsCurrencyTable
 */
class AddNetworkColumnsCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('md_currency', function (Blueprint $table) {
            $table->integer('networkId')->nullable()->unsigned()->default(null)->after('isNative');
            $table->foreign('networkId')->references('id')->on('md_network');

            $table->boolean('isDefault')->default(false)->after('networkId');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('md_currency', function (Blueprint $table) {
            $table->dropForeign(['networkId']);
            $table->dropColumn('networkId');

            $table->dropColumn('isDefault');
        });
    }
}
