<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateCityTable
 */
class CreateCityTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_city', function (Blueprint $table) {
            $table->increments('cityId');
            $table->integer('stateId')->unsigned();
            $table->string('name', 100)->nullable(false);
            $table->string('code', 10)->nullable(false);

            $table->foreign('stateId')->references('stateId')->on('md_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_city');
    }
}
