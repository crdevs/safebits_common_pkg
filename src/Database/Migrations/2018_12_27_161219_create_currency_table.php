<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Safebits\Common\Database\Migrations\MDMigration;

/**
 * Class CreateCurrencyTable
 */
class CreateCurrencyTable extends MDMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('md_currency', function (Blueprint $table) {
            $table->increments('currencyId');
            $table->string('name', 50)->nullable(false);
            $table->string('iso', 3)->nullable(false);
            $table->string('symbol', 5)->nullable(false);
            $table->string('type', 10)->nullable(false);
            $table->smallInteger('decimalPlaces')->nullable(false);
            $table->smallInteger('fullyConfirmedAt')->nullable();
            $table->string('qrCode')->nullable();
            $table->boolean('isActive')->nullable(false)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('md_currency');
    }
}
