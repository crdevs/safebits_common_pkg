<?php

namespace Safebits\Common\Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class MDSeeder
 * @package Safebits\Common\Database\Seeders
 */
class MDSeeder extends Seeder
{
    /**
     * @var string
     */
    protected $connection;

    /**
     * CommonSeeder constructor.
     */
    public function __construct()
    {
        $this->connection = config('safebits_common.connection', 'mysqlMd');
    }
}
