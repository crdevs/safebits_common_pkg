<?php

namespace Safebits\Common\Database\Seeders;

use Safebits\Common\Models\Client;

/**
 * Class ClientTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class ClientTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_client")->truncate();

        $clientList = [];
        $clientList[] = array(
            'name' => 'Disney',
            'acronym' => 'D',
            'identificationNumber' => '01'
        );

        foreach ($clientList as $clientItem) {
            $client = new Client($clientItem);
            $client->save();
        }
    }
}
