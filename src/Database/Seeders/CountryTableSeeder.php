<?php

namespace Safebits\Common\Database\Seeders;

/**
 * Class CountryTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class CountryTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_country")->truncate();

        // Load countries mysql script.
        $path = dirname(dirname(__FILE__)) . '/Scripts/countries_insert_query.sql';
        \DB::unprepared(file_get_contents($path));
    }
}
