<?php

namespace Safebits\Common\Database\Seeders;

use Safebits\Common\Models\Currency;
use Safebits\Common\Models\Network;

/**
 * Class CurrencyTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class CurrencyTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_currency")->truncate();

        //Note: almost all the "fullyConfirmedAt" values were taken from:  https://support.kraken.com/hc/en-us/articles/203325283-Cryptocurrency-deposit-processing-times
        $currencyList = [];
        $currencyList[] = array(
            'name' => 'Bitcoin',
            'providerName' => 'Bitcoin',
            'iso' => Currency::BTC_ISO,
            'symbol' => 'B',
            'decimalPlaces' => 8,
            'fullyConfirmedAt' => 6,
            'qrCode' => '{CURRENCY}:{ADDRESS}{AMOUNT}{MESSAGE}{LABEL}',
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'networkId' => Network::BTC_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'United States Dollar',
            'providerName' => 'United States Dollar',
            'iso' => Currency::USD_ISO,
            'symbol' => '$',
            'decimalPlaces' => 2,
            'fullyConfirmedAt' => null,
            'type' => Currency::FIAT,
            'isActive' => true,
        );
        $currencyList[] = array(
            'name' => 'Litecoin',
            'providerName' => 'Litecoin',
            'iso' => Currency::LTC_ISO,
            'symbol' => 'L',
            'decimalPlaces' => 8,
            'fullyConfirmedAt' => 6,
            'qrCode' => '{CURRENCY}:{ADDRESS}{AMOUNT}{MESSAGE}{LABEL}',
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'networkId' => Network::LTC_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'Bitcoin Cash',
            'providerName' => 'Bitcoin Cash',
            'iso' => Currency::BCH_ISO,
            'symbol' => 'C',
            'decimalPlaces' => 8,
            'fullyConfirmedAt' => 6,
            'qrCode' => '{CURRENCY}:{ADDRESS}{AMOUNT}{MESSAGE}{LABEL}',
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'networkId' => Network::BCH_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'Ethereum',
            'providerName' => 'Ethereum',
            'iso' => Currency::ETH_ISO,
            'symbol' => 'E',
            'decimalPlaces' => 18,
            'fullyConfirmedAt' => 12,
            'qrCode' => '{CURRENCY}:{ADDRESS}',
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'networkId' => Network::ERC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'Mexican Peso',
            'providerName' => 'Mexican Peso',
            'iso' => Currency::MXN_ISO,
            'symbol' => 'Mex$',
            'decimalPlaces' => 2,
            'fullyConfirmedAt' => null,
            'qrCode' => null,
            'type' => Currency::FIAT,
            'isActive' => true,
        );
        $currencyList[] = array(
            'name' => 'Euro',
            'providerName' => 'Euro',
            'iso' => Currency::EUR_ISO,
            'symbol' => '€',
            'decimalPlaces' => 2,
            'fullyConfirmedAt' => null,
            'qrCode' => null,
            'type' => Currency::FIAT,
            'isActive' => true,
        );
        $currencyList[] = array(
            'name' => 'Canadian Dollar',
            'providerName' => 'Canadian Dollar',
            'iso' => Currency::CAD_ISO,
            'symbol' => 'C$',
            'decimalPlaces' => 2,
            'fullyConfirmedAt' => null,
            'qrCode' => null,
            'type' => Currency::FIAT,
            'isActive' => true,
        );
        $currencyList[] = array(
            'name' => 'Costa Rican Colón',
            'providerName' => 'Costa Rican Colón',
            'iso' => Currency::CRC_ISO,
            'symbol' => '₡',
            'decimalPlaces' => 2,
            'fullyConfirmedAt' => null,
            'qrCode' => null,
            'type' => Currency::FIAT,
            'isActive' => true,
        );
        $currencyList[] = array(
            'name' => 'Japanese Yen',
            'providerName' => 'Japanese Yen',
            'iso' => Currency::JPY_ISO,
            'symbol' => '¥',
            'decimalPlaces' => 2,
            'fullyConfirmedAt' => null,
            'qrCode' => null,
            'type' => Currency::FIAT,
            'isActive' => true,
        );
        $currencyList[] = array(
            'name' => 'British Pound Sterling',
            'providerName' => 'British Pound Sterling',
            'iso' => Currency::GBP_ISO,
            'symbol' => '£',
            'decimalPlaces' => 2,
            'fullyConfirmedAt' => null,
            'qrCode' => null,
            'type' => Currency::FIAT,
            'isActive' => true,
        );
        $currencyList[] = array(
            'name' => 'Ripple',
            'providerName' => 'Ripple',
            'iso' => Currency::XRP_ISO,
            'symbol' => 'XRP',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 1,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => false,
        );
        $currencyList[] = array(
            'name' => 'Solana',
            'providerName' => 'Solana',
            'iso' => Currency::SOL_ISO,
            'symbol' => 'SOL',
            'decimalPlaces' => 18,
            'fullyConfirmedAt' => 1,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => false,
        );
        $currencyList[] = array(
            'name' => 'Cardano',
            'providerName' => 'Cardano',
            'iso' => Currency::ADA_ISO,
            'symbol' => 'ADA',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 15,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => false,
        );
        $currencyList[] = array(
            'name' => 'Dogecoin',
            'providerName' => 'Dogecoin',
            'iso' => Currency::DOGE_ISO,
            'symbol' => 'DOGE',
            'decimalPlaces' => 8,
            'fullyConfirmedAt' => 40,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => false,
        );
        $currencyList[] = array(
            'name' => 'Tether',
            'providerName' => 'Tether',
            'iso' => Currency::USDT_ISO,
            'symbol' => '₮',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => true,
            'networkId' => Network::ERC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'Binance USD',
            'providerName' => 'Binance USD',
            'iso' => Currency::BUSD_ISO,
            'symbol' => 'BUSD',
            'decimalPlaces' => 18,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => true,
            'networkId' => Network::ERC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'Wrapped Bitcoin',
            'providerName' => 'Wrapped Bitcoin',
            'iso' => Currency::WBTC_ISO,
            'symbol' => 'WBTC',
            'decimalPlaces' => 18,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => true,
            'networkId' => Network::ERC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'USD Coin',
            'providerName' => 'USD Coin',
            'iso' => Currency::USDC_ISO,
            'symbol' => 'USDC',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => true,
            'networkId' => Network::ERC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'currencyId' => Currency::TRX,
            'name' => 'TRX',
            'providerName' => 'Tron',
            'iso' => Currency::TRX_ISO,
            'symbol' => 'TRX',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => false,
            'networkId' => Network::TRC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'currencyId' => Currency::USDT_TRC20,
            'name' => 'Tether',
            'providerName' => 'Tether',
            'iso' => Currency::USDT_TRC20_ISO,
            'symbol' => '₮',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => false,
            'networkId' => Network::TRC20_NETWORK_ID,
            'isDefault' => false
        );
        $currencyList[] = array(
            'name' => 'BNB',
            'providerName' => 'BinanceCoin',
            'iso' => Currency::BNB_ISO,
            'symbol' => 'BNB',
            'decimalPlaces' => 18,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => true,
            'networkId' => Network::ERC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'name' => 'Euro Coin',
            'providerName' => 'Euro Coin',
            'iso' => Currency::EUROC_ISO,
            'symbol' => 'EUROC',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => true,
            'networkId' => Network::ERC20_NETWORK_ID,
            'isDefault' => true
        );
        $currencyList[] = array(
            'currencyId' => Currency::USDC_TRC20,
            'name' => 'USD Coin',
            'providerName' => 'USD Coin',
            'iso' => Currency::USDC_TRC20_ISO,
            'symbol' => 'USDC',
            'decimalPlaces' => 6,
            'fullyConfirmedAt' => 12,
            'qrCode' => null,
            'type' => Currency::CRYPTO,
            'isActive' => true,
            'isNative' => true,
            'isERC20' => false,
            'networkId' => Network::TRC20_NETWORK_ID,
            'isDefault' => false
        );

        foreach ($currencyList as $currencyItem) {
            $currency = new Currency($currencyItem);
            $currency->save();
        }
    }
}
