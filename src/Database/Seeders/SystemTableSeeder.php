<?php

namespace Safebits\Common\Database\Seeders;

use Safebits\Common\Models\System;

/**
 * Class SystemTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class SystemTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_system")->truncate();

        $system = new System();
        $data = array(
            array('systemId' => System::SYSTEM_CORE, 'name' => 'Safebits Core', 'url' => 'http://core.safebits.test', 'path' => 'core'),
            array('systemId' => System::SYSTEM_WALLET, 'name' => 'Safebits Wallet', 'url' => 'http://wallet.safebits.test', 'path' => 'wallet'),
            array('systemId' => System::SYSTEM_EXCHANGE, 'name' => 'Safebits Exchange', 'url' => 'http://exchange.safebits.test', 'path' => 'exchange'),
            array('systemId' => System::SYSTEM_GATEWAY, 'name' => 'Safebits Gateway', 'url' => 'http://gateway.safebits.test', 'path' => 'gateway'),
            array('systemId' => System::SYSTEM_SAFEBITS_ADMIN, 'name' => 'Safebits Admin', 'url' => 'http://api.admin.safebits.test', 'path' => 'admin'),
            array('systemId' => System::SYSTEM_EXTERNAL, 'name' => 'External', 'url' => '', 'path' => '')
        );

        $system::insert($data);
    }
}
