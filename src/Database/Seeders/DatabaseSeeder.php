<?php

namespace Safebits\Common\Database\Seeders;

use Illuminate\Support\Facades\Schema;
use Safebits\Common\Models\ClientDivisionCredential;

/**
 * Class DatabaseSeeder
 * @package Safebits\Common\Database\Seeders
 */
class DatabaseSeeder extends MDSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();

        Schema::connection($this->connection)->disableForeignKeyConstraints();

        $this->call(ClientTableSeeder::class);
        $this->call(ClientDivisionTableSeeder::class);
        $this->call(ClientDivisionCredential::class);
        $this->call(SystemTableSeeder::class);
        $this->call(NetworkTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);

        Schema::connection($this->connection)->enableForeignKeyConstraints();
    }
}
