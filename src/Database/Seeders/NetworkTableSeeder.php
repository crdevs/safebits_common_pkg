<?php

namespace Safebits\Common\Database\Seeders;

use Safebits\Common\Models\Network;

/**
 * Class NetworksTableSeeder
 */
class NetworkTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $networks = [
            ['name'=>'Bitcoin', 'code' => Network::BTC_NETWORK_CODE, 'id' => Network::BTC_NETWORK_ID],
            ['name'=>'Ethereum', 'code' => Network::ERC20_NETWORK_CODE, 'id' => Network::ERC20_NETWORK_ID],
            ['name'=>'Litecoin', 'code' => Network::LTC_NETWORK_CODE, 'id' => Network::LTC_NETWORK_ID],
            ['name'=>'Bitcoin Cash', 'code' => Network::BCH_NETWORK_CODE, 'id' => Network::BCH_NETWORK_ID],
            ['name'=>'Tron', 'code' => Network::TRC20_NETWORK_CODE, 'id' => Network::TRC20_NETWORK_ID],
        ];

        foreach ($networks as $network) {
            $dbNetwork = new Network();
            $dbNetwork->id = $network['id'];
            $dbNetwork->name = $network['name'];
            $dbNetwork->code = $network['code'];
            $dbNetwork->save();
        }
    }
}
