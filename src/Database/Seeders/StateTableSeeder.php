<?php

namespace Safebits\Common\Database\Seeders;

/**
 * Class StateTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class StateTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_state")->truncate();

        // Load states from mysql script
        $path = dirname(dirname(__FILE__)) . '/Scripts/states_insert_query.sql';
        \DB::unprepared(file_get_contents($path));
    }
}
