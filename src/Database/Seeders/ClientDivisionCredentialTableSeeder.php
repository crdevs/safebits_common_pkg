<?php

namespace Safebits\Common\Database\Seeders;

use Safebits\Common\Models\ClientDivision;
use Safebits\Common\Models\ClientDivisionCredential;

/**
 * Class ClientDivisionCredentialTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class ClientDivisionCredentialTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_client_division_credential")->truncate();

        $clientDivisionList = ClientDivision::all();

        foreach ($clientDivisionList as $clientDivision) {
            $clientDivisionCredential = new ClientDivisionCredential();
            $clientDivisionCredential->clientDivisionId = $clientDivision->clientDivisionId;
            $clientDivisionCredential->key = "key" . $clientDivision->clientDivisionId;
            $clientDivisionCredential->secret = "secret" . $clientDivision->clientDivisionId;
            $clientDivisionCredential->save();
        }
    }
}
