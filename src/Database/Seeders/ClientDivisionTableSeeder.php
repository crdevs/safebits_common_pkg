<?php

namespace Safebits\Common\Database\Seeders;

use Safebits\Common\Models\ClientDivision;

/**
 * Class ClientDivisionTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class ClientDivisionTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_client_division")->truncate();

        $clientDivisionList[] = array(
            'clientId' => 1,
            'name' => 'Pixar',
            'acronym' => 'P',
            'identificationNumber' => '01'
        );
        $clientDivisionList[] = array(
            'clientId' => 1,
            'name' => 'ESPN',
            'acronym' => 'E',
            'identificationNumber' => '02'
        );

        foreach ($clientDivisionList as $clientDivisionItem) {
            $clientDivision = new ClientDivision($clientDivisionItem);
            $clientDivision->save();
        }
    }
}
