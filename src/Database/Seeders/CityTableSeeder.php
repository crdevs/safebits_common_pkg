<?php

namespace Safebits\Common\Database\Seeders;

/**
 * Class CityTableSeeder
 * @package Safebits\Common\Database\Seeders
 */
class CityTableSeeder extends MDSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::connection($this->connection)->table("md_city")->truncate();

        // Load cities mysql script.
        $path = dirname(dirname(__FILE__)) . '/Scripts/cities_insert_query.sql';
        \DB::unprepared(file_get_contents($path));
    }
}
