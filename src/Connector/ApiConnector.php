<?php

namespace Safebits\Common\Connector;

use Safebits\Common\Models\ClientDivision;
use Safebits\Common\Constants\CommonConstants;
use Safebits\Common\Exceptions\MisconfigurationException;
use Safebits\Common\Models\System;
use Safebits\Connector\Connector\SBConnector;

/**
 * Class ApiConnector
 * @package Safebits\Common\Connector
 */
class ApiConnector extends SBConnector
{
    /**
     * Overwrite in Specific Connectors and set its value to System::SYSTEM_abc
     */
    const SYSTEM_ID = 0;

    /**
     * @var
     */
    protected $systemURL;

    /**
     * @var $previousRequest
     */
    protected $previousRequest;

    /**
     * @var $nonce
     */
    private $nonce;

    /**
     * Connector constructor.
     * @param $systemTo
     * @throws \Exception
     */
    public function __construct()
    {
        $this->previousRequest = request();
        $this->setNonce();
        $this->setSystemURL();
    }

    /**
     * @param $endpoint
     * @param array $params
     * @param array $customHeaders
     * @return \Safebits\Connector\Models\SBConnectorErrorResponse|\Safebits\Connector\Models\SBConnectorSuccessResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Safebits\Connector\Exceptions\ApiRequestException
     * @throws \Safebits\Connector\Exceptions\BadResponseExceptionSB
     */
    public function post($endpoint, $params = [], $customHeaders = [])
    {
        $url = $this->systemURL . $endpoint;
        $response = parent::post($url, $params, $customHeaders);
        return $response;
    }

    /**
     * @param $endpoint
     * @param array $params
     * @param array $customHeaders
     * @return \Safebits\Connector\Models\SBConnectorErrorResponse|\Safebits\Connector\Models\SBConnectorSuccessResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Safebits\Connector\Exceptions\ApiRequestException
     * @throws \Safebits\Connector\Exceptions\BadResponseExceptionSB
     */
    public function get($endpoint, $params = [], $customHeaders = [])
    {
        $url = $this->systemURL . $endpoint;
        $response = parent::get($url, $params, $customHeaders);
        return $response;
    }

    /**
     * Sets the request endpoint according to the given systems
     *
     * @throws MisconfigurationException
     */
    private function setSystemURL()
    {
        $system = System::where('systemId', static::SYSTEM_ID)->first();

        if (!$system) {
            throw new MisconfigurationException('No system configured for ' . static::class);
        }

        $this->systemURL = $system->url;
    }

    /**
     *
     */
    private function setNonce()
    {
        $this->nonce = time();
    }

    /**
     * @param $endpoint
     * @param $params
     * @param $customHeaders
     * @return array
     * @throws \Exception
     */
    protected function getHeaders($endpoint, $params, $customHeaders)
    {
        // These are the default headers
        $requestHeaders = [];
        $requestHeaders['Accept'] = 'application/json';

        // Sets the division id header
        if (array_key_exists(CommonConstants::X_CRYPTO_DIVISION, $customHeaders)) {
            $divisionId = $customHeaders[CommonConstants::X_CRYPTO_DIVISION];
        } else {
            if ($this->previousRequest && $this->previousRequest->header(CommonConstants::X_CRYPTO_DIVISION)) {
                $divisionId = $this->previousRequest->header(CommonConstants::X_CRYPTO_DIVISION);
            } else {
                $divisionId = ClientDivision::first()->clientDivisionId;
            }
        }

        $requestHeaders[CommonConstants::X_CRYPTO_DIVISION] = $divisionId;
        $requestHeaders[CommonConstants::X_CRYPTO_SYSTEM] = static::SYSTEM_ID;
        $requestHeaders[CommonConstants::X_CRYPTO_NONCE] = $this->nonce;

        // Both header arrays are merged into a single array
        $finalHeaders = array_merge($requestHeaders, $customHeaders);

        return $finalHeaders;
    }
}
