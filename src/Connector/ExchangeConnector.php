<?php

namespace Safebits\Common\Connector;

use Safebits\Common\Models\System;

/**
 * Class ExchangeConnector
 * @package Safebits\Common\Connector
 */
class ExchangeConnector extends ApiConnector
{
    const SYSTEM_ID = System::SYSTEM_EXCHANGE;
}
