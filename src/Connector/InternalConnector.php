<?php
/**
 * Andrés Bolaños
 */

namespace Safebits\Common\Connector;

use Safebits\Common\Constants\CommonConstants;
use Safebits\Common\Exceptions\MisconfigurationException;
use Safebits\Common\Models\System;
use Safebits\Connector\Connector\SBConnector;

/**
 * Class InternalConnector
 * @package Safebits\Common\Connector
 */
class InternalConnector extends SBConnector
{

    /**
     * @var $url
     */
    protected $url;

    /**
     * @var $nonce
     */
    private $nonce;

    /**
     * InternalConnector constructor.
     * @param $url
     * @throws MisconfigurationException
     */
    public function __construct($url)
    {
        $this->url = $url;
        $this->setNonce();
    }

    /**
     * @param $url
     * @param array $params
     * @param array $customHeaders
     * @return \Safebits\Connector\Models\SBConnectorErrorResponse|\Safebits\Connector\Models\SBConnectorSuccessResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Safebits\Connector\Exceptions\ApiRequestException
     * @throws \Safebits\Connector\Exceptions\BadResponseExceptionSB
     */
    public function post($url, $params = [], $customHeaders = [])
    {
        $response = parent::post($url, $params, $customHeaders);
        return $response;
    }

    /**
     * @param $url
     * @param array $params
     * @param array $customHeaders
     * @return \Safebits\Connector\Models\SBConnectorErrorResponse|\Safebits\Connector\Models\SBConnectorSuccessResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Safebits\Connector\Exceptions\ApiRequestException
     * @throws \Safebits\Connector\Exceptions\BadResponseExceptionSB
     */
    public function get($url, $params = [], $customHeaders = [])
    {
        $response = parent::get($url, $params, $customHeaders);
        return $response;
    }

    /**
     * @param $endpoint
     * @param $params
     * @param $customHeaders
     * @return array
     * @throws \Exception
     */
    protected function getHeaders($endpoint, $params, $customHeaders)
    {
        // These are the default headers
        $requestHeaders = [];
        $requestHeaders[CommonConstants::X_CRYPTO_SYSTEM] = System::SYSTEM_GATEWAY;
        $requestHeaders[CommonConstants::X_CRYPTO_NONCE] = $this->nonce;

        // Both header arrays are merged into a single array
        $finalHeaders = array_merge($requestHeaders, $customHeaders);

        return $finalHeaders;
    }

    /**
     *
     */
    private function setNonce()
    {
        $this->nonce = time();
    }

    /**
     * @param $responseContent
     * @param $HTTPStatusCode
     * @param $HTTPHeaders
     * @return \Safebits\Connector\Models\SBConnectorErrorResponse|\Safebits\Connector\Models\SBConnectorSuccessResponse
     * @throws \Exception
     */
    protected function checkApiResponse($responseContent, $HTTPStatusCode, $HTTPHeaders)
    {
        $jsonResponse = '';
        $responseRaw = '';
        if ($responseContent) {
            $responseRaw = $responseContent;
            $jsonResponse = json_decode(json_encode($responseRaw), true);
        }

        if ($jsonResponse) {
            return response()->json($jsonResponse, $HTTPStatusCode, $HTTPHeaders);
        } else {
            throw new \Exception("Invalid Response. $responseRaw");
        }
    }
}
