<?php

namespace Safebits\Common\Connector;

use Safebits\Common\Models\System;

/**
 * Class CoreConnector
 * @package Safebits\Common\Connector
 */
class CoreConnector extends ApiConnector
{
    const SYSTEM_ID = System::SYSTEM_CORE;
}
