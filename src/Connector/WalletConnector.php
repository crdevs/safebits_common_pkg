<?php

namespace Safebits\Common\Connector;

use Safebits\Common\Models\System;

/**
 * Class WalletConnector
 * @package Safebits\Common\Connector
 */
class WalletConnector extends ApiConnector
{
    /**
     * WalletConnector constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $systemTo = System::SYSTEM_WALLET;
        parent::__construct($systemTo);
    }
}
