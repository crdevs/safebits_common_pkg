<?php

namespace Safebits\Common\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Safebits\Common\Models\MDModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\MDModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\MDModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\MDModel query()
 * @mixin \Eloquent
 */
class MDModel extends Model
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * Connection to master data base
     * @var $connection
     */
    protected $connection;

    /**
     * MDModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = config('safebits_common.connection', 'mysqlMd');
    }
}
