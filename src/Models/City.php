<?php

namespace Safebits\Common\Models;

/**
 * Safebits\Common\Models\City
 *
 * @property int $cityId
 * @property int $stateId
 * @property string $name
 * @property string|null $code
 * @property-read \Safebits\Common\Models\State $state
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\City query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\City whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\City whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\City whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\City whereStateId($value)
 * @mixin \Eloquent
 */
class City extends MDModel
{
    /**
     * Set the custom primary key  name
     * @var string
     */
    protected $primaryKey = 'cityId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * City constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_city');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'stateId', 'stateId');
    }
}
