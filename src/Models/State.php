<?php

namespace Safebits\Common\Models;

/**
 * Safebits\Common\Models\State
 *
 * @property int $stateId
 * @property int $countryId
 * @property string $name
 * @property string|null $code
 * @property-read \Illuminate\Database\Eloquent\Collection|\Safebits\Common\Models\City[] $cities
 * @property-read \Safebits\Common\Models\Country $country
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\State newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\State newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\State query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\State whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\State whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\State whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\State whereStateId($value)
 * @mixin \Eloquent
 */
class State extends MDModel
{
    /**
     * Set the custom primary key  name
     * @var string
     */
    protected $primaryKey = 'stateId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * State constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_state');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->hasOne(Country::class, 'countryId', 'countryId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany(City::class, 'stateId', 'stateId');
    }
}
