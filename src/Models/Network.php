<?php

namespace Safebits\Common\Models;

use Safebits\Common\Models\Currency;
use App\Exceptions\InvalidNetworkException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Network\Network
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Currency[] $currencies
 * @property-read int|null $currencies_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Network\Network newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Network\Network newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Network\Network query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Network\Network whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Network\Network whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Network\Network whereName($value)
 * @mixin \Eloquent
 */
class Network extends MDModel
{
    const BTC_NETWORK_CODE = 'BTC';
    const BTC_NETWORK_ID = 1;

    const ERC20_NETWORK_CODE = 'ERC20';
    const ERC20_NETWORK_ID = 2;

    const LTC_NETWORK_CODE = 'LTC';
    const LTC_NETWORK_ID = 3;

    const BCH_NETWORK_CODE = 'BCH';
    const BCH_NETWORK_ID = 4;

    const TRC20_NETWORK_CODE = 'TRC20';
    const TRC20_NETWORK_ID = 5;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_network');
        $this->timestamps = false;
    }

    /**
     * @return HasMany
     */
    public function currencies()
    {
        return $this->hasMany(Currency::class);
    }
}
