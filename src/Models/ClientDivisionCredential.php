<?php

namespace Safebits\Common\Models;

use Illuminate\Database\Eloquent\Model;
use Safebits\Common\Security\ClientDivisionEncryptable;

/**
 * Safebits\Common\Models\ClientDivisionCredential
 *
 * @property int $clientDivisionCredentialId
 * @property int $clientDivisionId
 * @property string $key
 * @property mixed|null $secret
 * @property int $isActive
 * @property \Illuminate\Support\Carbon $createdAt
 * @property \Illuminate\Support\Carbon $updatedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential whereClientDivisionCredentialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential whereClientDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential whereSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ClientDivisionCredential whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientDivisionCredential extends MDModel
{
    use ClientDivisionEncryptable;

    /**
     * Set the custom primary key  name
     * @var string
     */
    protected $primaryKey = 'clientDivisionId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Encryptable columns.
     * @var array
     */
    protected $encryptable = ['secret'];

    /**
     * ClientDivisionCredentials constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_client_division_credential');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientDivision()
    {
        return $this->belongsTo(ClientDivision::class, 'clientDivisionId', 'clientDivisionId');
    }
}
