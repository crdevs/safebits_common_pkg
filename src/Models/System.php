<?php

namespace Safebits\Common\Models;

/**
 * Safebits\Common\Models\System
 *
 * @property int $systemId
 * @property string $name
 * @property string $url
 * @property string $path
 * @property int $isActive
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System whereSystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\System whereUrl($value)
 * @mixin \Eloquent
 */
class System extends MDModel
{
    const SYSTEM_CORE = 1;
    const SYSTEM_WALLET = 2;
    const SYSTEM_EXCHANGE = 3;
    const SYSTEM_GATEWAY = 4;
    const SYSTEM_SAFEBITS_ADMIN = 5;
    const SYSTEM_EXTERNAL = 100;

    /**
     * @var string
     */
    protected $primaryKey = 'systemId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * System constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_system');
    }
}
