<?php

namespace Safebits\Common\Models;

/**
 * Safebits\Common\Models\Client
 *
 * @property int $clientId
 * @property string $name
 * @property string $acronym
 * @property string $identificationNumber
 * @property-read \Illuminate\Database\Eloquent\Collection|\Safebits\Common\Models\ClientDivision[] $clientDivisions
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Client whereAcronym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Client whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Client whereIdentificationNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Client whereName($value)
 * @mixin \Eloquent
 */
class Client extends MDModel
{
    /**
     * Set the custom primary key  name
     * @var string
     */
    protected $primaryKey = 'clientId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Client constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientDivisions()
    {
        return $this->hasMany(ClientDivision::class, 'clientId', 'clientId');
    }
}
