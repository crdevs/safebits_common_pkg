<?php

namespace Safebits\Common\Models;

/**
 * Safebits\Common\Models\Country
 *
 * @property int $countryId
 * @property string|null $name
 * @property string $iso2
 * @property string|null $iso3
 * @property string|null $numCode
 * @property-read \Illuminate\Database\Eloquent\Collection|\Safebits\Common\Models\State[] $states
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country whereIso2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country whereIso3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Country whereNumCode($value)
 * @mixin \Eloquent
 */
class Country extends MDModel
{
    /**
     * Set the custom primary key  name
     * @var string
     */
    protected $primaryKey = 'countryId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Country constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(State::class, 'countryId', 'countryId');
    }
}
