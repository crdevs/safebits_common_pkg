<?php

namespace Safebits\Common\Models;

/**
 * Safebits\Common\Models\ClientDivision
 *
 * @property int $clientDivisionId
 * @property int $clientId
 * @property string $name
 * @property string $acronym
 * @property string $identificationNumber
 * @property-read \Safebits\Common\Models\Client $client
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision whereAcronym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision whereClientDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision whereIdentificationNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\ClientDivision whereName($value)
 * @mixin \Eloquent
 */
class ClientDivision extends MDModel
{
    /**
     * Set the custom primary key  name
     * @var string
     */
    protected $primaryKey = 'clientDivisionId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * ClientDivision constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_client_division');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'clientId', 'clientId');
    }

    /**
     * @return mixed
     */
    public function clientDivisionCredential()
    {
        return $this->belongsTo(ClientDivisionCredential::class, 'clientDivisionId', 'clientDivisionId');
    }
}
