<?php

namespace Safebits\Common\Models;

use Safebits\Common\Exceptions\InvalidCurrencyNetworkException;
use Safebits\Common\Models\Network;
use Illuminate\Database\Eloquent\Model;

/**
 * Safebits\Common\Models\Currency
 *
 * @property int $currencyId
 * @property string $name
 * @property string $iso
 * @property string $symbol
 * @property string $type
 * @property int $decimalPlaces
 * @property int|null $fullyConfirmedAt
 * @property string|null $qrCode
 * @property int $isActive
 * @property int $isNative
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereDecimalPlaces($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereFullyConfirmedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereIsNative($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereQrCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereSymbol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Common\Models\Currency whereType($value)
 * @mixin \Eloquent
 */
class Currency extends MDModel
{
    /**
     * Constants definition for registered currencies
     */
    const BTC = 1;
    /**
     * Constants definition for registered currencies
     */
    const BTC_ISO = 'BTC';
    /**
     * Constants definition for registered currencies
     */
    const USD = 2;
    /**
     * Constants definition for registered currencies
     */
    const USD_ISO = 'USD';
    /**
     * Constants definition for registered currencies
     */
    const LTC = 3;
    /**
     * Constants definition for registered currencies
     */
    const LTC_ISO = 'LTC';
    /**
     * Constants definition for registered currencies
     */
    const BCH = 4;
    /**
     * Constants definition for registered currencies
     */
    const BCH_ISO = 'BCH';
    /**
     * Constants definition for registered currencies
     */
    const ETH = 5;
    /**
     * Constants definition for registered currencies
     */
    const ETH_ISO = 'ETH';
    /**
     * Constants definition for registered currencies
     */
    const MXN = 6;
    /**
     * Constants definition for registered currencies
     */
    const MXN_ISO = 'MXN';
    /**
     * Constants definition for registered currencies
     */
    const EUR = 7;
    /**
     * Constants definition for registered currencies
     */
    const EUR_ISO = 'EUR';
    /**
     * Constants definition for registered currencies
     */
    const CAD = 8;
    /**
     * Constants definition for registered currencies
     */
    const CAD_ISO = 'CAD';
    /**
     * Constants definition for registered currencies
     */
    const CRC = 9;
    /**
     * Constants definition for registered currencies
     */
    const CRC_ISO = 'CRC';
    /**
     * Constants definition for registered currencies
     */
    const JPY = 10;
    /**
     * Constants definition for registered currencies
     */
    const JPY_ISO = 'JPY';
    /**
     * Constants definition for registered currencies
     */
    const GBP = 11;
    /**
     * Constants definition for registered currencies
     */
    const GBP_ISO = 'GBP';
    /**
     * Constants definition for registered currencies
     */
    const XRP = 12;
    /**
     * Constants definition for registered currencies
     */
    const XRP_ISO = 'XRP';
    /**
     * Constants definition for registered currencies
     */
    const SOL = 13;
    /**
     * Constants definition for registered currencies
     */
    const SOL_ISO = 'SOL';
    /**
     * Constants definition for registered currencies
     */
    const ADA = 14;
    /**
     * Constants definition for registered currencies
     */
    const ADA_ISO = 'ADA';
    /**
     * Constants definition for registered currencies
     */
    const DOGE = 15;
    /**
     * Constants definition for registered currencies
     */
    const DOGE_ISO = 'DOGE';
    /**
     * Constants definition for registered currencies
     */
    const USDT = 16;
    /**
     * Constants definition for registered currencies
     */
    const USDT_ISO = 'USDT';
    /**
     * Constants definition for registered currencies
     */
    const BUSD = 17;
    /**
     * Constants definition for registered currencies
     */
    const BUSD_ISO = 'BUSD';
    /**
     * Constants definition for registered currencies
     */
    const WBTC = 18;
    /**
     * Constants definition for registered currencies
     */
    const WBTC_ISO = 'WBTC';
    /**
     * Constants definition for registered currencies
     */
    const USDC = 19;
    /**
     * Constants definition for registered currencies
     */
    const USDC_ISO = 'USDC';
    /**
     * Constants definition for registered currencies
     */
    const TRX = 20;
    /**
     * Constants definition for registered currencies
     */
    const TRX_ISO = 'TRX';
    /**
     * Constants definition for registered currencies
     */
    const USDT_TRC20 = 21;
    /**
     * Constants definition for registered currencies
     */
    const USDT_TRC20_ISO = 'USDT';
    /**
     * Constants definition for registered currencies
     */
    const BNB = 22;
    /**
     * Constants definition for registered currencies
     */
    const BNB_ISO = 'BNB';
    /**
     * Constants definition for registered currencies
     */
    const EUROC = 23;
    /**
     * Constants definition for registered currencies
     */
    const EUROC_ISO = 'EUROC';
    /**
     * Constants definition for registered currencies
     */
    const USDC_TRC20 = 24;
    /**
     * Constants definition for registered currencies
     */
    const USDC_TRC20_ISO = 'USDC';
    /**
     * Constants definition for registered currencies
     */
    const CRYPTO = 'CRYPTO';
    /**
     * Constants definition for registered currencies
     */
    const FIAT = 'FIAT';

    /**
     * Set the custom primary key  name
     * @var string
     */
    protected $primaryKey = 'currencyId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Currency constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('md_currency');
    }

    /**
     * @param $currencyIso
     * @param $networkCode
     * @return mixed
     * @throws InvalidCurrencyNetworkException
     */
    public static function loadByIsoAndNetwork($currencyIso, $networkCode=null)
    {
        if ($networkCode) {
            $network = Network::where('code', $networkCode)->first();
            if (!$network) {
                throw new InvalidCurrencyNetworkException("The network $networkCode is not supported");
            }

            $currency = self::where('iso', $currencyIso)->where('networkId', $network->id)->first();
            if (!$currency) {
                throw new InvalidCurrencyNetworkException("The network $networkCode is not supported for $currencyIso");
            }

            return $currency;
        }

        else {
            return self::defaultNetworkCurrency($currencyIso);
        }
    }

    /**
     * @param $currencyIso
     * @return mixed
     * @throws InvalidCurrencyNetworkException
     */
    public static function defaultNetworkCurrency($currencyIso)
    {
        $defaultCurrency = Currency::where('iso', $currencyIso)->where('isDefault', 1)->first();

        if (!$defaultCurrency) {
            throw new InvalidCurrencyNetworkException("The currency $currencyIso does not have a default network configured");
        }

        return $defaultCurrency;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function network()
    {
        return $this->belongsTo(Network::class, 'networkId', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function crypto()
    {
        return self::where('type', self::CRYPTO)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function fiat()
    {
        return self::where('type', self::FIAT)->get();
    }
}
