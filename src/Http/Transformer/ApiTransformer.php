<?php

/**
 * Created by ecordero.
 */

namespace Safebits\Common\Http\Transformer;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;

/**
 * Class ApiTransformer
 * @package App\Api
 */
class ApiTransformer
{
    /**
     * @param $item
     * @param $transformer
     * @param array $includes
     * @return array
     */
    protected function transformItem($item, $transformer, $includes = array())
    {
        $fractal = new Manager();
        $fractal->setSerializer(new ArraySerializer());

        if (is_array($includes) && count($includes) > 0) {
            $fractal->parseIncludes($includes);
        }

        $resource = new Item($item, $transformer);
        $data = $fractal->createData($resource)->toArray();

        return $data;
    }

    /**
     * @param $items
     * @param $transformer
     * @param array $includes
     * @return mixed
     */
    protected function transformCollection($items, $transformer, $includes = array())
    {
        $fractal = new Manager();
        $fractal->setSerializer(new ArraySerializer());

        if (is_array($includes) && count($includes) > 0) {
            $fractal->parseIncludes($includes);
        }

        $resource = new Collection($items, $transformer);
        $data = $fractal->createData($resource)->toArray();

        return $data['data'];
    }
}
