<?php

namespace Safebits\Common\Http\Controller;

use Safebits\Common\Helpers\AuthHelper;

/**
 * Class ExternalApiController
 * @package Safebits\Common\Http\Controller
 */
class ExternalApiController extends ApiController
{
    /**
     * @return array
     * @throws \Safebits\Common\Exceptions\InvalidAccessKeyException
     */
    protected function getRequestClientDivisionData()
    {
        return AuthHelper::getExternalRequestClientDivision();
    }
}
