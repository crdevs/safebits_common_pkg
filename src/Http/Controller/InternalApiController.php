<?php

namespace Safebits\Common\Http\Controller;

use Safebits\Common\Helpers\AuthHelper;

/**
 * Class InternalApiController
 * @package Safebits\Common\Http\Controller
 */
class InternalApiController extends ApiController
{
    protected function getRequestClientDivisionData()
    {
        return AuthHelper::getInternalRequestClientDivision();
    }
}
