<?php

namespace Safebits\Common\Http\Controller;

use App\Framework\Http\Controller\ApplicationController;
use Illuminate\Http\Request;
use Safebits\Common\Http\Responses\ApiErrorResponse;
use Safebits\Common\Http\Responses\ApiSuccessResponse;
use Safebits\Common\Http\Transformer\ApiTransformer;
use Safebits\Common\Constants\CommonConstants;

/**
 * Class ApiController
 * @package Safebits\Common\Http\Controller
 */
class ApiController extends ApplicationController
{
    /**
     * @var null
     */
    protected $apiTransformer = null;

    /**
     * @var int
     */
    protected $clientDivision;

    /**
     * @var int
     */
    protected $clientDivisionId;

    /**
     * @var int
     */
    protected $clientDivisionCredential;

    /**
     * @param Request $request
     * @throws \Safebits\Common\Exceptions\InactiveMerchantException
     * @throws \Safebits\Common\Exceptions\InvalidAccessKeyException
     */
    public function __construct(Request $request)
    {
        $this->apiTransformer = new ApiTransformer();

        if (env('APP_ENV') !== 'production') {
            $this->middleware('logDBQueries');
        }

        $this->setRequestClientDivisionData();
    }

    /**
     * @return void
     * @throws \Safebits\Common\Exceptions\InactiveMerchantException
     * @throws \Safebits\Common\Exceptions\InvalidAccessKeyException
     */
    protected function setRequestClientDivisionData()
    {
        [$clientDivision, $clientDivisionCredential] = $this->getRequestClientDivisionData();

        $this->clientDivision = $clientDivision;
        $this->clientDivisionId = $clientDivision->clientDivisionId;
        $this->clientDivisionCredential = $clientDivisionCredential;
    }

    protected function getRequestClientDivisionData()
    {
        throw new \Exception("Method " . __FUNCTION__ . " not implemented on " . __CLASS__);
    }

    /**
     * @param null $payload
     * @param int $httpStatus
     * @param null $message
     * @param array $httpHeaders
     * @param int $httpOptions
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuccessResponse($payload = null, $httpStatus = CommonConstants::HTTP_STATUS_200, $message = null, $httpHeaders = [], $httpOptions = 0)
    {
        $apiSuccessResponse = new ApiSuccessResponse();
        $apiSuccessResponse->status = 1;
        $apiSuccessResponse->message = isset($message) ? $message : "Ok";
        $apiSuccessResponse->payload = $payload;

        return response()->json($apiSuccessResponse, $httpStatus, $httpHeaders, $httpOptions);
    }

    /**
     * @param $message
     * @param $reason
     * @param $reasonCode
     * @param int $httpStatus
     * @param array $httpHeaders
     * @param int $httpOptions
     * @return \Illuminate\Http\JsonResponse
     */
    public function getErrorResponse($message, $reason, $reasonCode, $httpStatus = CommonConstants::HTTP_STATUS_400, $httpHeaders = [], $httpOptions = 0)
    {
        $apiErrorResponse = new ApiErrorResponse();
        $apiErrorResponse->status = 0;
        $apiErrorResponse->message = $message;
        $apiErrorResponse->reasonCode = $reasonCode;
        $apiErrorResponse->reason = $reason;

        return response()->json($apiErrorResponse, $httpStatus, $httpHeaders, $httpOptions);
    }
}
