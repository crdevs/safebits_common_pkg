<?php

namespace Safebits\Common\Http\Responses;

/**
 * Class ApiErrorResponse
 * @package Safebits\Common\Http\Responses
 */
class ApiErrorResponse
{
    /**
     * Status signaling the failure of the request.
     *
     * @var integer
     */
    public $status = null;

    /**
     * High level user-friendly error message.
     *
     * @var string
     */
    public $message = null;

    /**
     * Scenario specific failure message.
     *
     * @var integer
     */
    public $reasonCode = null;

    /**
     * Description of the reason code message
     *
     * @var string
     */
    public $reason = null;
}
