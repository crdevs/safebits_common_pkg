<?php

namespace Safebits\Common\Http\Responses;

/**
 * Class ApiSuccessResponse
 * @package Safebits\Common\Http\Responses
 */
class ApiSuccessResponse
{
    /**
     * Status signaling the success of the request.
     *
     * @var integer
     */
    public $status = null;

    /**
     * Optional message returned by the service
     * @var null
     */
    public $message = null;

    /**
     * Optional payload returned by the service
     * @var mixed
     */
    public $payload = null;
}
