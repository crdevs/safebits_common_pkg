<?php

/**
 * Andrés  Bolaños
 */

namespace Safebits\Common\Helpers;

use Illuminate\Http\Request;
use Safebits\Common\Constants\CommonConstants;
use Safebits\Common\Exceptions\MisconfigurationException;
use \Safebits\Common\Exceptions\InvalidAccessKeyException;
use \Safebits\Common\Exceptions\InactiveClientDivisionException;
use Safebits\Common\Models\ClientDivision;
use Safebits\Common\Models\ClientDivisionCredential;

/**
 * Class AuthHelper
 * @package Safebits\Common\Helpers
 */
class AuthHelper
{
    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function getKeyHeader(Request $request)
    {
        $header = null;
        if ($request->hasHeader(CommonConstants::X_SAFEBITS_KEY)) {
            $header = $request->header(CommonConstants::X_SAFEBITS_KEY);
        } elseif ($request->hasHeader(CommonConstants::X_CRYPTO_KEY)) {
            $header = $request->header(CommonConstants::X_CRYPTO_KEY);
        } elseif ($request->hasHeader(CommonConstants::X_ANALYTICS_KEY)) {
            $header = $request->header(CommonConstants::X_ANALYTICS_KEY);
        }

        return $header;
    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function getSignatureHeader(Request $request)
    {
        $header = null;
        if ($request->hasHeader(CommonConstants::X_SAFEBITS_SIGNATURE)) {
            $header = $request->header(CommonConstants::X_SAFEBITS_SIGNATURE);
        } elseif ($request->hasHeader(CommonConstants::X_CRYPTO_SIGNATURE)) {
            $header = $request->header(CommonConstants::X_CRYPTO_SIGNATURE);
        } elseif ($request->hasHeader(CommonConstants::X_ANALYTICS_SIGNATURE)) {
            $header = $request->header(CommonConstants::X_ANALYTICS_SIGNATURE);
        }

        return $header;
    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function getNonceHeader(Request $request)
    {
        $header = null;
        if ($request->hasHeader(CommonConstants::X_SAFEBITS_NONCE)) {
            $header = $request->header(CommonConstants::X_SAFEBITS_NONCE);
        } elseif ($request->hasHeader(CommonConstants::X_CRYPTO_NONCE)) {
            $header = $request->header(CommonConstants::X_CRYPTO_NONCE);
        } elseif ($request->hasHeader(CommonConstants::X_ANALYTICS_NONCE)) {
            $header = $request->header(CommonConstants::X_ANALYTICS_NONCE);
        }

        return $header;
    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function getCalbackHeader(Request $request)
    {
        $header = null;
        if ($request->hasHeader(CommonConstants::X_SAFEBITS_CALLBACK_ID)) {
            $header = $request->header(CommonConstants::X_SAFEBITS_CALLBACK_ID);
        } elseif ($request->hasHeader(CommonConstants::X_CRYPTO_CALLBACK_ID)) {
            $header = $request->header(CommonConstants::X_CRYPTO_CALLBACK_ID);
        } elseif ($request->hasHeader(CommonConstants::X_ANALYTICS_CALLBACK_ID)) {
            $header = $request->header(CommonConstants::X_ANALYTICS_CALLBACK_ID);
        }

        return $header;
    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function getAccessHeader(Request $request)
    {
        // Nginx sets header X-Crypto-Access = "direct" if Core is accessed through public api
        $accessHeader = $request->header(CommonConstants::X_CRYPTO_ACCESS);

        // Header was not set correctly, added as a php fpm param instead
        if (!$accessHeader) {
            $accessHeader = $request->server->get(CommonConstants::X_CRYPTO_ACCESS);
        }

        return $accessHeader;
    }

    /**
     * @param Request $request
     * @param $secret
     * @return string
     * @throws MisconfigurationException
     */
    public static function getRequestSignature(Request $request, $secret)
    {
        return self::calculateMessageSignature(AuthHelper::getNonceHeader($request), $request->getPathInfo(), $request->method(), $request->all(), $secret);
    }

    /**
     * @param $nonce
     * @param $uriPath
     * @param $requestMethod
     * @param $requestParams
     * @param $secret
     * @return string
     * @throws MisconfigurationException
     */
    public static function calculateMessageSignature($nonce, $uriPath, $requestMethod, $requestParams, $secret)
    {
        $uriPath = utf8_encode($uriPath);

        // Depending on the method the message is different
        $requestData = array();
        if (strtolower($requestMethod) == "post") {
            if (is_array($requestParams)) {
                $requestData = json_encode($requestParams);
            }

        } else {
            if (is_array($requestParams)) {
                $requestData = json_encode($requestParams);
            } else {
                // Gets the query string for the request. url encoded.
                $requestData = http_build_query($requestParams);
            }
        }

        $encoding = mb_detect_encoding($requestData, 'UTF-8');
        if ($encoding != 'UTF-8') {
            throw new MisconfigurationException('The encoding used in the request is invalid.');
        }
        
        $requestData = strtolower(hash('sha256', $requestData, false));
        $msg = $uriPath . $nonce . $requestData;
        $signature = hash_hmac("sha512", $msg, $secret);

        return $signature;
    }

    /**
     * @return array
     * @throws InactiveClientDivisionException
     * @throws InvalidAccessKeyException
     */
    public static function getInternalRequestClientDivision()
    {
        $divisionId = request()->header(CommonConstants::X_CRYPTO_DIVISION);

        if (!$divisionId) {
            throw new InvalidAccessKeyException();
        }

        $clientDivision = ClientDivision::where('clientDivisionId', $divisionId)->first();

        if (!$clientDivision) {
            throw new InvalidAccessKeyException();
        }

        if(!$clientDivision->clientDivisionCredential()->exists() || !$clientDivision->clientDivisionCredential->isActive) {
            throw new InactiveClientDivisionException();
        }

        return [$clientDivision, $clientDivision->clientDivisionCredential];
    }

    /**
     * @return array
     * @throws InactiveClientDivisionException
     * @throws InvalidAccessKeyException
     */
    public static function getExternalRequestClientDivision()
    {
        $key = AuthHelper::getKeyHeader(request());

        if (!$key) {
            throw new InvalidAccessKeyException();
        }

        $clientDivisionCredential = ClientDivisionCredential::where(['key' => $key, 'isActive' => 1])->first();
        if (!$clientDivisionCredential) {
            throw new InvalidAccessKeyException();
        }

        if(!$clientDivisionCredential->isActive) {
            throw new InactiveClientDivisionException();
        }

        return [$clientDivisionCredential->clientDivision, $clientDivisionCredential];
    }
}
