<?php

namespace Safebits\Common\Commands;

use Illuminate\Console\Command;
use Illuminate\Encryption\Encrypter;
use Safebits\Configuration\Models\SystemConfiguration;

/**
 * Class MigrateEncryptorKey
 * @package Safebits\Common\Commands
 */
class CreateEncryptorKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sb:create-encryptor-key';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the encryptor key that will be used to encrypt the credentials. Only for local usage!';

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        // Get Key
        $key = $this->getEncryptorKey();

        // Make SystemConfiguration class
        $this->makeKeySystemConfiguration($key);
    }

    private function getEncryptorKey()
    {
        $encryptorKeyFile = base_path().'/../.encryptor.key';
        $key = '';

        // First option for key: find it in encryptor.key file in working-directory/
        if (file_exists($encryptorKeyFile)) {
            $key = file_get_contents($encryptorKeyFile);
        }

        // Second option for key: create a new one, save the new key in encryptor.key file, so we don't create a new one next time (for next project)
        if (!$key) {
            $key = $this->generateRandomKey();
            file_put_contents($encryptorKeyFile, $key);
        }

        return $key;
    }

    /**
     * Generate a random key for the application.
     *
     * @return string
     */
    protected function generateRandomKey()
    {
        return 'base64:'.base64_encode(
            Encrypter::generateKey($this->laravel['config']['app.cipher'])
        );
    }

    private function makeKeySystemConfiguration($key)
    {
        $systemConfiguration = new SystemConfiguration();
        $systemConfiguration->tag = 'APP_CREDS_ENCRYPT_KEY';
        $systemConfiguration->value = $key;
        $systemConfiguration->type = 'string';
        $systemConfiguration->encrypted = 1;
        $systemConfiguration->description = 'Used to encrypt credentials.';
        $systemConfiguration->save();
    }
}
