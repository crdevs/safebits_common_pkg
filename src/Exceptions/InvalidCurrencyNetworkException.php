<?php

namespace Safebits\Common\Exceptions;

/**
 * Class CommonException
 * @package Safebits\Common\Exceptions
 */
class InvalidCurrencyNetworkException extends \Exception
{
    /**
     * CommonException constructor.
     * @param $message
     * @param $code
     * @param null $previous
     */
    public function __construct($message, $code, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
