<?php

/**
 * Andrés Bolaños
 */

namespace Safebits\Common\Exceptions;

use Safebits\Common\Constants\CommonConstants;

/**
 * Class InvalidAccessSignatureException
 * @package Safebits\Common\Exceptions
 */
class InvalidAccessSignatureException extends CommonException
{
    const EXCEPTION_MESSAGE = CommonConstants::INVALID_ACCESS_SIGNATURE_MESSAGE;
    const EXCEPTION_CODE = CommonConstants::INVALID_ACCESS_SIGNATURE_CODE;
}
