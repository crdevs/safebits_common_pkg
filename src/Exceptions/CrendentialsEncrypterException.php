<?php

namespace Safebits\Common\Exceptions;

use Safebits\Common\Constants\CommonConstants;

/**
 * Class CrendentialsEncrypterException
 * @package Safebits\Common\Exceptions
 */
class CrendentialsEncrypterException extends CommonException
{
    const EXCEPTION_MESSAGE = CommonConstants::CREDENTIAL_ENCRYPTER_EXCEPTION_MESSAGE;
    const EXCEPTION_CODE = CommonConstants::CREDENTIAL_ENCRYPTER_EXCEPTION_CODE;
}
