<?php

namespace Safebits\Common\Exceptions;

use Safebits\Common\Constants\CommonConstants;

/**
 * Class CommonException
 * @package Safebits\Common\Exceptions
 */
class CommonException extends \Exception
{
    const EXCEPTION_MESSAGE = CommonConstants::UNKNOWN_EXCEPTION_MESSAGE;
    const EXCEPTION_CODE = CommonConstants::UNKNOWN_EXCEPTION_CODE;

    /**
     * CommonException constructor.
     * @param $message
     * @param $code
     * @param null $previous
     */
    public function __construct($message = '', $code = 0, $previous = null)
    {
        $message = $message ? : static::EXCEPTION_MESSAGE;
        $code = $code ? : static::EXCEPTION_CODE;

        parent::__construct($message, $code, $previous);
    }
}
