<?php

namespace Safebits\Common\Exceptions;

use Safebits\Common\Constants\CommonConstants;

/**
 * Class InactiveClientDivisionException
 * @package Safebits\Common\Exceptions
 */
class InactiveClientDivisionException extends CommonException
{
    const EXCEPTION_MESSAGE = CommonConstants::INACTIVE_CLIENT_DIVISION_MESSAGE;
    const EXCEPTION_CODE = CommonConstants::INACTIVE_CLIENT_DIVISION_CODE;
}
