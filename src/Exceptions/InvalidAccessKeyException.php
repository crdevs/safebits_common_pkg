<?php

namespace Safebits\Common\Exceptions;

use Safebits\Common\Constants\CommonConstants;

/**
 * Class InvalidAccessKeyException
 * @package Safebits\Common\Exceptions
 */
class InvalidAccessKeyException extends CommonException
{
    const EXCEPTION_MESSAGE = CommonConstants::INVALID_ACCESS_KEY_MESSAGE;
    const EXCEPTION_CODE = CommonConstants::INVALID_ACCESS_KEY_CODE;
}