<?php

namespace Safebits\Common\Exceptions;

use Safebits\Common\Constants\CommonConstants;

/**
 * Class InvalidAccessKeyException
 * @package Safebits\Common\Exceptions
 */
class InvalidRequestEncodingException extends CommonException
{
    const EXCEPTION_MESSAGE = CommonConstants::INVALID_REQUEST_ENCODING_MESSAGE;
    const EXCEPTION_CODE = CommonConstants::INVALID_REQUEST_ENCODING_CODE;
}