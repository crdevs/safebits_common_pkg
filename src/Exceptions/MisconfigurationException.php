<?php

namespace Safebits\Common\Exceptions;

use Safebits\Common\Constants\CommonConstants;

/**
 * Class MisconfigurationException
 * @package Safebits\Common\Exceptions
 */
class MisconfigurationException extends CommonException
{
    const EXCEPTION_MESSAGE = CommonConstants::MISCONFIGURATION_EXCEPTION_MESSAGE;
    const EXCEPTION_CODE = CommonConstants::MISCONFIGURATION_EXCEPTION_CODE;
}
