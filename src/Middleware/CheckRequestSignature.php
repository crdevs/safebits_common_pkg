<?php

namespace Safebits\Common\Middleware;

use Safebits\Common\Constants\CommonConstants;
use Safebits\Common\Exceptions\InvalidAccessKeyException;
use Safebits\Common\Exceptions\InvalidAccessSignatureException;
use Safebits\Common\Exceptions\InvalidRequestEncodingException;
use Safebits\Common\Helpers\AuthHelper;
use Safebits\Common\Models\ClientDivisionCredential;
use Closure;
use Illuminate\Http\Request;

/**
 * Class CheckRequestSignature
 * @package Safebits\Common\Middleware
 */
class CheckRequestSignature
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param $requestTypeId
     * @return mixed
     * @throws InvalidAccessKeyException
     * @throws InvalidAccessSignatureException
     * @throws InvalidRequestEncodingException
     */
    public function handle(Request $request, Closure $next)
    {
        if ( $this->checkSignature($request)) {
            return $next($request);
        }

        throw new InvalidAccessSignatureException();
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     * @throws InvalidAccessKeyException
     * @throws InvalidRequestEncodingException
     */
    private function checkSignature(Request $request)
    {
        $clientDivisionCredentials = $this->getDivisionCredential($request);

        if ($clientDivisionCredentials === null) {
            throw new InvalidAccessKeyException();
        }

        $signature = AuthHelper::getSignatureHeader($request);
        $messageSignature = AuthHelper::getRequestSignature($request, $clientDivisionCredentials->secret);

        if (env('SB_CHECK_API_SIGNATURES') == false && (env('APP_ENV') != 'production' || (env('APP_ENV') == 'production' && env('APP_IS_BETA') == 1))) {
            return true;
        }

        $signatureCheck = $signature === $messageSignature;

        if ($signatureCheck) {
            $request->headers->set(CommonConstants::X_CRYPTO_DIVISION, $clientDivisionCredentials->clientDivisionId);
        }

        return $signatureCheck;
    }

    /**
     * @param Request $request
     * @return ClientDivisionCredential|\Illuminate\Database\Eloquent\Model|null|object
     */
    protected function getDivisionCredential(Request $request)
    {
        $key = AuthHelper::getKeyHeader($request);
        if (!$key) {
            return null;
        }
        $clientDivisionCredentials = ClientDivisionCredential::where(['key' => $key, 'isActive' => 1])->first();
        if ($clientDivisionCredentials === null) {
            return null;
        }
        return $clientDivisionCredentials;
    }
}
