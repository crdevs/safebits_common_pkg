<?php

namespace Safebits\Common\Security;

use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Str;
use Safebits\Common\Exceptions\CommonException;
use Safebits\Configuration\Models\SystemConfiguration;

trait ClientDivisionEncryptable
{
    /**
     * @var
     */
    private static $encrypter;
    
    /**
     * @param $key
     * @return string
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (in_array($key, $this->encryptable)) {
            if (!$value) {
                $value = '';
            } else {
                $value = $this->decrypt($value);
            }
        }

        return $value;
    }

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable)) {
            $value = $this->encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }

    /**
     * @return mixed
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray(); // call the parent method

        foreach ($this->encryptable as $key) {

            if (isset($attributes[$key])) {
                $attributes[$key] = $this->decrypt($attributes[$key]);
            }
        }
        return $attributes;
    }

    /**
     * @param $value
     * @return mixed
     */
    private function encrypt($value)
    {
        $encrypter = $this->getEncrypter();
        return $encrypter->encrypt($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    private function decrypt($value)
    {
        $encrypter = $this->getEncrypter();
        return $encrypter->decrypt($value);
    }

    /**
     * @return Encrypter
     */
    private function getEncrypter()
    {
        if (!self::$encrypter) {
            $encrypterKey = $this->getEncrypterKey();

            // vendor/laravel/framework/src/Illuminate/Foundation/Console/KeyGenerateCommand.php
            // vendor/laravel/framework/src/Illuminate/Encryption/Encrypter.php:__construct()
            // https://laravel.com/docs/5.8/encryption#using-the-encrypter
            // AES-256-CBC
            $encrypterCypher = config('app.cipher');

            self::$encrypter = new Encrypter($encrypterKey, $encrypterCypher);

            if (!$encrypterKey) {
                throw new CredentialsEncrypterException();
            }
        }

        return self::$encrypter;
    }

    /**
     * @return string
     */
    private function getEncrypterKey()
    {
        $encrypterKey = SystemConfiguration::getConfigurationByTag('APP_CREDS_ENCRYPT_KEY');

        // Illuminate\Encryption\EncryptionServiceProvider
        // If the key starts with "base64:", we will need to decode the key before handing
        // it off to the encrypter. Keys may be base-64 encoded for presentation and we
        // want to make sure to convert them back to the raw bytes before encrypting.
        if (Str::startsWith($encrypterKey, 'base64:')) {
            $encrypterKey = base64_decode(substr($encrypterKey, 7));
        }

        if (!$encrypterKey) {
            throw new CommonException('No credential encryptor key found');
        }

        return $encrypterKey;
    }
}